#include <cstdint>
#include <cstring>
#include <seastar/core/app-template.hh>
#include <seastar/core/coroutine.hh>
#include <seastar/core/file.hh>
#include <seastar/core/future.hh>
#include <seastar/core/reactor.hh>
#include <seastar/core/sleep.hh>
#include <seastar/core/sstring.hh>
#include <seastar/core/thread.hh>
#include <seastar/core/when_all.hh>
#include <seastar/http/common.hh>
#include <seastar/http/function_handlers.hh>
#include <seastar/http/httpd.hh>
#include <seastar/util/defer.hh>

#include <iostream>
#include <list>
#include <mutex>
#include <syncstream>

#include <curl/curl.h>

#include <boost/unordered_map.hpp>

/*
 * You need to implement a simple persistent key/value store
 * Utilizing the seastar library for asynchronous execution and input/output operations, maximizing the usage of the disk CPU and memory.
 *    Dataset:
        Keys are utf-8 strings up to 255 bytes in length
        Values are utf-8 strings of unlimited length.
        Data is sharded among the CPU cores, based on they keys
    API
        Data is manipulated and accessed using http-based REST API.
        At a minimum the api should include the following operations:
            Insert / update of a single key/value pair
            Query of a single key, returning its value
            Query the sorted list of all keys
            Delete a single key (and its associated value)
    Cache
        Newly written data should be cached in memory
        Recently used data should be cached in memory
        The size of the cache is limited to some configurable limit
            Least recently used data should be evicted from the cache to make room for new data that was recently read or written
 */

std::string url_decode(const std::string& encoded)
{
    int output_length;
    const auto decoded_value = curl_easy_unescape(nullptr, encoded.c_str(), static_cast<int>(encoded.length()), &output_length);
    std::string result(decoded_value, output_length);
    curl_free(decoded_value);
    return result;
}

struct cache_data {
    seastar::sstring _key;
    seastar::sstring _value;
    size_t _page_nr;
};

struct sstring_hasher {
    size_t operator()(const seastar::sstring* str) const {
        return boost::hash<seastar::sstring>()(*str);
    }
};

struct sstring_comparator {
    bool operator()(const seastar::sstring* lhs, const seastar::sstring* rhs) const {
        return (*lhs == *rhs);
    }
};

class cache {
    boost::unordered_map<const seastar::sstring*, std::list<cache_data>::iterator, sstring_hasher, sstring_comparator> _lookup;
    std::list<cache_data> _data;
    size_t _cache_size = 0;
    const size_t _cache_limit;
public:
    cache(size_t cache_limit) : _cache_limit(cache_limit) {
        _lookup.reserve(cache_limit);
    }

    bool add(seastar::sstring& key, seastar::sstring& value, size_t page_nr) {
        auto it = _lookup.find(&key);
        if (it != _lookup.end()) {
            it->second->_value = std::move(value);
            _data.splice(_data.begin(), _data, it->second);
            return true;
        }

        _data.push_front({std::move(key), std::move(value)});
        _lookup.emplace(&_data.front()._key, _data.begin());
        _cache_size += key.size() + value.size();
        if (_cache_size > _cache_limit)
            return remove_lru();
        return true;
    }

    bool update(const seastar::sstring& key, seastar::sstring& value) {
        auto it = _lookup.find(&key);
        if (it != _lookup.end()) {
            it->second->_value = std::move(value);
            _data.splice(_data.begin(), _data, it->second);
            return true;
        }
        return false;
    }

    bool get_page(const seastar::sstring& key, size_t& value) {
        auto it = _lookup.find(&key);
        if (it != _lookup.end()) {
            _data.splice(_data.begin(), _data, it->second);
            value = it->second->_page_nr;
            return true;
        }
        return false;
    }

    bool get(const seastar::sstring& key, seastar::sstring& value) {
        auto it = _lookup.find(&key);
        if (it != _lookup.end()) {
            _data.splice(_data.begin(), _data, it->second);
            value = it->second->_value;
            return true;
        }
        return false;
    }

    bool remove(const seastar::sstring& key) {
        auto it = _lookup.find(&key);
        if (it != _lookup.end()) {
            _cache_size -= (it->second->_key.size() + it->second->_value.size());
            _data.erase(it->second);
            _lookup.erase(it);
            return true;
        }
        return false;
    }

    bool remove_lru() {
        if (_data.size() && _cache_size > _cache_limit) {
            size_t res = _lookup.erase(&_data.back()._key);
            assert(res == 1);
            _cache_size -= (_data.back()._key.size() + _data.back()._value.size());
            _data.pop_back();
        }
        return true;
    }

    bool empty() const {
        return _data.empty();
    }
};

class storage {
    using temp_buffer = seastar::temporary_buffer<char>;
    static constexpr size_t PAGE_SIZE = 4096;
    static constexpr size_t KEY_SIZE  = 256;
    static constexpr size_t B_ORDER   = 15;

    enum node_type {
        leaf = 1 << 0,
        root = 1 << 1,
    };

    struct __attribute__ ((packed)) page_header {
        uint8_t _flags = 0;
        uint8_t _nkeys = 0;
        uint8_t _key_sizes[B_ORDER];
        size_t  _pointers[B_ORDER + 1];
        size_t  _page = 0;
        size_t  _parent = 0;
    };

    struct __attribute__ ((packed)) key_page {
        uint8_t _flags = 0;
        uint8_t _nkeys = 0;
        uint8_t _key_sizes[B_ORDER];
        size_t  _pointers[B_ORDER + 1];
        size_t  _page = 0;
        size_t  _parent = 0;
        char    _padding[KEY_SIZE - sizeof(page_header)];
        char    _keys[B_ORDER][KEY_SIZE];

        void set_leaf(bool value) {
            if (value) {
                _flags |= node_type::leaf;
            } else {
                _flags &= ~node_type::leaf;
            }
        }

        bool is_leaf() const {
            return _flags & node_type::leaf;
        }

        bool empty() const {
            return _nkeys == 0;
        }

        bool is_root() const {
            return _page == 0;
        }

        bool insert_needs_split() const {
            return (_nkeys + 1) >= B_ORDER;
        }

        seastar::sstring key(size_t index) {
            assert(index < _nkeys);
            seastar::sstring res;
            res.resize(_key_sizes[index]);
            strncpy(res.begin(), _keys[index], _key_sizes[index]);
            return res;
        }

        bool has(const seastar::sstring& key) {
            for (uint8_t i = 0; i < _nkeys; i++) {
                if (std::strcmp(_keys[i], key.begin()) == 0) {
                    return true;
                }
            }
            return false;
        }

        int locate_index(const seastar::sstring& key) {
            for (uint8_t i = 0; i < _nkeys; i++) {
                int res = std::strcmp(_keys[i], key.begin());
                if (res >= 0) {
                    return res == 0 ? -1 : i;
                }
            }
            return _nkeys;
        }

        int insert(const seastar::sstring& key) {
            assert (_nkeys + 1 <= B_ORDER);
            int i = locate_index(key);
            if (i >= 0) {
                move_right(i);
                _key_sizes[i] = key.size();
                strncpy(_keys[i], key.begin(), key.size());
                _keys[i][_key_sizes[i]] = '\0';
            }
            return i;
        }

        void append_from(const key_page& p, size_t start, size_t end) {
            for (size_t i = start; i < end; i++) {
                _pointers[_nkeys + 1] = p._pointers[i + 1];
                _key_sizes[_nkeys] = p._key_sizes[i];
                strncpy(_keys[_nkeys], p._keys[i], p._key_sizes[i]);
                _nkeys++;
            }
        }

        void remove(size_t start, size_t end) {
            size_t len = _nkeys - end;
            _nkeys = start;
            for (size_t i = 0; i < len; i++) {
                _pointers[_nkeys + 1] = _pointers[end + i + 1];
                _key_sizes[_nkeys] = _key_sizes[end + i];
                strncpy(_keys[_nkeys], _keys[end + i], _key_sizes[end + i]);
                _nkeys++;
            }
        }

        void pop_front() {
            for (size_t i = 0; i < _nkeys - 1; i++) {
                _pointers[i] = _pointers[i + 1];
                _key_sizes[i] = _key_sizes[i + 1];
                strncpy(_keys[i], _keys[i + 1], _key_sizes[i + 1]);
            }
            _nkeys--;
            _pointers[_nkeys] = _pointers[_nkeys + 1];
        }

        void move_right(size_t start) {
            assert (_nkeys + 1 <= B_ORDER);
            for (size_t j = _nkeys; j > start; j--) {
                strncpy(_keys[j], _keys[j - 1], _key_sizes[j - 1]);
                _pointers[j + 1] = _pointers[j];
                _key_sizes[j] = _key_sizes[j - 1];
            }
            _pointers[start + 1] = _pointers[start];
            _nkeys++;
        }

    };

    struct __attribute__ ((packed)) data_page {
        uint16_t _size = 0;
        uint16_t _flags = 0;
        char _data[PAGE_SIZE - 2 * sizeof(uint16_t)];
    };

    cache _cache;
    const seastar::sstring _keys_file_name;
    const seastar::sstring _data_file_name;

    size_t _page_nr = 0;

    seastar::file _keys_file;
    seastar::file _data_file;

    seastar::future<bool> write_page(temp_buffer p, const seastar::sstring& file_name, size_t page_index) {
        try {
            if (file_name == _keys_file_name)
                co_await _keys_file.dma_write(PAGE_SIZE * page_index, p.get(), p.size());
            else
                co_await _data_file.dma_write(PAGE_SIZE * page_index, p.get(), p.size());
            co_return true;
        } catch (...) {
            std::cout << std::current_exception() << std::endl;
            co_return false;
        }
    }

    seastar::future<temp_buffer> read_page(size_t page_index, size_t page_count, const seastar::sstring& file_name) {
        auto buffer = temp_buffer::aligned(PAGE_SIZE, PAGE_SIZE * page_count);
        if (file_name == _keys_file_name)
            co_await _keys_file.dma_read(PAGE_SIZE * page_index, buffer.get_write(), buffer.size());
        else
            co_await _data_file.dma_read(PAGE_SIZE * page_index, buffer.get_write(), buffer.size());
        co_return std::move(buffer);
    }

    seastar::future<seastar::sstring> read_value(size_t offset) {
        auto buffer = temp_buffer::aligned(PAGE_SIZE, PAGE_SIZE);
        size_t page_index = offset / PAGE_SIZE;
        co_await _data_file.dma_read(PAGE_SIZE * page_index, buffer.get_write(), PAGE_SIZE);
        size_t key_size = buffer[offset % PAGE_SIZE];
        seastar::sstring value;
        value.resize(key_size);
        //TODO: multi page long keys
        memcpy(value.begin(), &buffer.get()[offset % PAGE_SIZE + sizeof(size_t)], key_size);
        co_return value;
    }

    seastar::future<std::vector<seastar::sstring>> list_internal(size_t page_nr, size_t depth) {
        auto buffer = co_await read_page(page_nr, 1, _keys_file_name);
        std::vector<seastar::sstring> res;
        res.reserve(B_ORDER);
        if (buffer.size()) {
            key_page *p = (key_page*)buffer.get();
            if (p->is_leaf()) {
                for (size_t i = 0; i < p->_nkeys; i++) {
                    res.push_back(p->key(i));
                }
            } else {
                std::vector<size_t> pages(p->_nkeys + 1);
                for (size_t i = 0; i <= p->_nkeys; i++) {
                    pages[i] = p->_pointers[i];
                }
                buffer.release();
                for (size_t i = 0; i < pages.size(); i++) {
                    auto tmp = co_await list_internal(pages[i], depth);
                    res.insert(res.end(), tmp.begin(), tmp.end());
                }
            }
        }
        co_return res;
    }

    seastar::future<bool> find_in_leaf(temp_buffer buffer, const seastar::sstring& key, seastar::sstring& value) {
        key_page *p = (key_page*)buffer.get_write();
        assert (p->is_leaf());
        for (int i = 0; i < p->_nkeys; i++) {
            if (std::strcmp(p->_keys[i], key.begin()) == 0) {
                value = co_await read_value(p->_pointers[i + 1]);
                co_return true;
            }
        }
        co_return false;
    }

    seastar::future<bool> remove_in_leaf(temp_buffer buffer, const seastar::sstring& key) {
        key_page *p = (key_page*)buffer.get_write();
        assert (p->is_leaf());
        for (int i = 0; i < p->_nkeys; i++) {
            if (std::strcmp(p->_keys[i], key.begin()) == 0) {
                p->remove(i, i + 1);
                bool res = co_await write_page(std::move(buffer), _keys_file_name, p->_page);
                co_return res;
            }
        }
        co_return false;
    }

    seastar::future<temp_buffer> find_leaf(const seastar::sstring& key, size_t page_index) {
        auto buffer = co_await read_page(page_index, 1, _keys_file_name);
        key_page *p = (key_page*)buffer.get_write();
        if (p->is_leaf() || p->empty()) {
            co_return std::move(buffer);
        } else {
            for (uint8_t i = 0; i < p->_nkeys; i++) {
                if (std::strcmp(p->_keys[i], key.begin()) >= 0) {
                    auto res = co_await find_leaf(key, p->_pointers[i]);
                    co_return res;
                }
            }
            auto res = co_await find_leaf(key, p->_pointers[p->_nkeys]);
            co_return std::move(res);
        }
    }

    seastar::future<size_t> append_value(const seastar::sstring value) {
        size_t value_size = value.size() + 1;
        auto metadata = co_await read_page(0, 1, _data_file_name);
        size_t offset = 0;
        memcpy(&offset, metadata.get(), sizeof(size_t));
        size_t page_count = 1 + ((offset % PAGE_SIZE) + value_size + sizeof(size_t)) / PAGE_SIZE;
        auto buffer = co_await read_page(offset / PAGE_SIZE, page_count, _data_file_name); 
        memcpy(buffer.get_write() + (offset % PAGE_SIZE), &value_size, sizeof(size_t));
        memcpy(buffer.get_write() + (offset % PAGE_SIZE) + sizeof(size_t), value.begin(), value_size);
        size_t new_offset = offset + sizeof(size_t) + value.size() + 1;
        memcpy(metadata.get_write(), &new_offset, sizeof(size_t));
        co_await write_page(std::move(buffer), _data_file_name, offset / PAGE_SIZE); 
        co_await write_page(std::move(metadata), _data_file_name, 0);
        co_return offset;
    }

    temp_buffer get_buffer() const {
        auto buffer = temp_buffer::aligned(PAGE_SIZE, PAGE_SIZE);
        memset(buffer.get_write(), 0, PAGE_SIZE);
        return std::move(buffer);
    }

    seastar::future<> update_children(key_page* p) {
        size_t newpage = p->_page;
        for (size_t i = 0; i <= p->_nkeys; i++) {
            auto buffer = co_await read_page(p->_pointers[i], 1, _keys_file_name);
            key_page *child = (key_page*)buffer.get_write();
            child->_parent = newpage;
            bool res = co_await write_page(std::move(buffer), _keys_file_name, child->_page);
        }
        co_return;
    }

    seastar::future<size_t> insert(temp_buffer buffer, const seastar::sstring& key, const seastar::sstring& value, size_t page_value) {
        size_t res = std::numeric_limits<size_t>::max();
        key_page *p = (key_page*)buffer.get_write();
        assert (p->_nkeys <= B_ORDER);
        if (p->insert_needs_split()) {
            int index = p->insert(key);
            if (index < 0) {
                co_return false;
            }
            if (p->is_leaf()) {
                size_t value_offset = co_await append_value(value);
                p->_pointers[index + 1] = value_offset;
            } else {
                p->_pointers[index + 1] = page_value;
            }
            if (p->is_root()) {
                auto newbuffer0 = get_buffer();
                auto newbuffer1 = get_buffer();
                key_page *newp0 = (key_page*)newbuffer0.get_write();
                key_page *newp1 = (key_page*)newbuffer1.get_write();
                newp0->_page = ++_page_nr;
                newp1->_page = ++_page_nr;
                newp0->append_from(*p, 0, (B_ORDER + 1) / 2);
                newp1->append_from(*p, (B_ORDER + 1) / 2, p->_nkeys);
                newp0->_pointers[0] = p->_pointers[0];
                p->remove(0, p->_nkeys);
                p->insert(newp1->key(0));
                p->_pointers[0] = newp0->_page;
                p->_pointers[1] = newp1->_page;
                if (p->is_leaf()) {
                    newp0->set_leaf(true);
                    newp1->set_leaf(true);
                    p->set_leaf(false);
                } else {
                    newp1->pop_front();
                    co_await update_children(newp0);
                    co_await update_children(newp1);
                }
                size_t newp0_page = newp0->_page;
                size_t newp1_page = newp1->_page;
                size_t p_page = p->_page;
                auto r = co_await seastar::when_all(
                    write_page(std::move(newbuffer0), _keys_file_name, newp0_page),
                    write_page(std::move(newbuffer1), _keys_file_name, newp1_page),
                    write_page(std::move(buffer), _keys_file_name, p_page));
                res = index < (B_ORDER + 1) / 2 ? newp0_page : newp1_page;
            } else {
                auto newbuffer = get_buffer();
                key_page *newp = (key_page*)newbuffer.get_write();
                newp->_page = ++_page_nr;
                newp->append_from(*p, (B_ORDER + 1) / 2, p->_nkeys);
                newp->_parent = p->_parent;
                p->remove((B_ORDER + 1) / 2, p->_nkeys);
                seastar::sstring npkey = newp->key(0);
                if (p->is_leaf()) {
                    newp->set_leaf(true);
                } else {
                    newp->pop_front();
                    co_await update_children(newp);
                }
                size_t p_page = p->_page;
                size_t newp_page = newp->_page;
                size_t newp_parent = newp->_parent;
                co_await write_page(std::move(newbuffer), _keys_file_name, newp_page);
                co_await write_page(std::move(buffer), _keys_file_name, p_page);
                auto parent_page = co_await read_page(newp_parent, 1, _keys_file_name);
                co_await insert(std::move(parent_page), npkey, value, newp_page);
                res = index < (B_ORDER + 1) / 2 ? p_page : newp_page;
            }
        } else {
            int index = p->insert(key);
            if (index < 0) {
                co_return false;
            }
            if (p->is_leaf()) {
                size_t value_offset = co_await append_value(value);
                p->_pointers[index + 1] = value_offset;
            } else {
                p->_pointers[index + 1] = page_value;
            }
            size_t p_page = p->_page;
            co_await write_page(std::move(buffer), _keys_file_name, p_page);
            res = p->_page;
        }
        co_return res;
    }

public:
    storage(size_t cache_limit) : _cache(cache_limit),
            _keys_file_name(fmt::format("keys_{}.ddb", seastar::this_shard_id())),
            _data_file_name(fmt::format("data_{}.ddb", seastar::this_shard_id())) {
        static_assert(sizeof(key_page) <= PAGE_SIZE, "sizeof(key_page) > PAGE_SIZE");
    }

    ~storage() {
    }

    seastar::future<> init() {
        bool exists = co_await seastar::file_exists(_keys_file_name);
        if (!exists) {
            _keys_file = co_await seastar::open_file_dma(_keys_file_name, seastar::open_flags::rw | seastar::open_flags::create);
            if (_keys_file) {
                auto buffer = get_buffer();
                key_page *p = (key_page*)buffer.get_write();
                p->set_leaf(true);
                co_await _keys_file.dma_write(0, buffer.get(), buffer.size());
            }
        } else {
            _keys_file = co_await seastar::open_file_dma(_keys_file_name, seastar::open_flags::rw);
            size_t file_size = co_await _keys_file.size();
            _page_nr = file_size % PAGE_SIZE;
        }
        bool exists_data = co_await seastar::file_exists(_data_file_name);
        if (!exists_data) {
            _data_file = co_await seastar::open_file_dma(_data_file_name, seastar::open_flags::rw | seastar::open_flags::create);
            if (_data_file) {
                auto buffer = get_buffer();
                size_t header = PAGE_SIZE;
                memcpy(buffer.get_write(), &header, sizeof(header));
                buffer.get_write()[PAGE_SIZE - 1] = '\n';
                co_await _data_file.dma_write(0, buffer.get(), buffer.size());
            }
        } else {
            _data_file = co_await seastar::open_file_dma(_data_file_name, seastar::open_flags::rw);
        }
        co_return;
    }

    seastar::future<bool> add(seastar::sstring key, seastar::sstring value) {
        size_t page_number_hint = 0;
        temp_buffer buffer;
        if (_cache.get_page(key, page_number_hint)) {
            buffer = co_await read_page(page_number_hint, 1, _keys_file_name);
        }
        else
            buffer = co_await find_leaf(key, 0);
        if (buffer.size()) {
            size_t page = co_await insert(std::move(buffer), key, value, 0);
            if (page != std::numeric_limits<size_t>::max()) {
                _cache.add(key, value, page);
                co_return true;
            }
        }
        co_return false;
    }

    seastar::future<bool> get(const seastar::sstring key, seastar::sstring& value) {
        if (_cache.get(key, value))
            co_return true;
        auto buffer = co_await find_leaf(key, 0);
        bool res = co_await find_in_leaf(std::move(buffer), key, value);
        co_return res;
    }

    seastar::future<bool> remove(const seastar::sstring key) {
        size_t page_number_hint = 0;
        temp_buffer buffer;
        if (_cache.get_page(key, page_number_hint)) {
            buffer = co_await read_page(page_number_hint, 1, _keys_file_name);
        }
        else
            buffer = co_await find_leaf(key, 0);
        bool res = false;
        if (buffer) {
            res = co_await remove_in_leaf(std::move(buffer), key);
            _cache.remove(key);
        }
        co_return res;
    }

    seastar::future<std::vector<seastar::sstring>> list() {
        return list_internal(0, 0);
    }
};

class sharded_storage {
    seastar::distributed<storage>& _peers;

    inline unsigned get_cpu(const seastar::sstring& key) {
        return sstring_hasher()(&key) % seastar::smp::count;
    }

public:
    sharded_storage(seastar::distributed<storage>& peers, size_t cache_limit) :
        _peers(peers)
    {
    }

    seastar::future<bool> add(seastar::sstring& key, seastar::sstring& value) {
        auto cpu = get_cpu(key);
        if (seastar::this_shard_id() == cpu) {
            bool res = co_await _peers.local().add(key, value);
            co_return res;
        } else {
            bool res = co_await _peers.invoke_on(cpu, &storage::add, std::ref(key), std::ref(value));
            co_return res;
        }
        co_return true;
    }

    seastar::future<bool> get(const seastar::sstring& key, seastar::sstring& value) {
        auto cpu = get_cpu(key);
        if (seastar::this_shard_id() == cpu) {
            return _peers.local().get(key, value);
        }
        return _peers.invoke_on(cpu, &storage::get, std::ref(key), std::ref(value));
    }

    seastar::future<bool> remove(const seastar::sstring key) {
        auto cpu = get_cpu(key);
        if (seastar::this_shard_id() == cpu) {
            return _peers.local().remove(std::ref(key));
        }
        return _peers.invoke_on(cpu, &storage::remove, std::ref(key));
    }

    template <typename Result, typename Addend = Result>
        class vector_adder {
        private:
            Result _result;
        public:
            seastar::future<> operator()(const Addend& value) {
                _result.insert(_result.end(), value.begin(), value.end());
                return seastar::make_ready_future<>();
            }
            Result get() && {
                return std::move(_result);
            }
        };
    seastar::future<seastar::sstring> list() const {
        std::vector<seastar::sstring> r = co_await _peers.map_reduce(vector_adder<std::vector<seastar::sstring>>(), &storage::list);
        std::sort(r.begin(), r.end());
        seastar::sstring res;
        for (const auto& s : r)
            res += s + " ";
        co_return res;
    }
};

using namespace seastar::http;
class key_handler : public seastar::httpd::handler_base {
    sharded_storage& _storage;
public:
    key_handler(sharded_storage& storage) : _storage(storage) {}

    seastar::future<std::unique_ptr<reply> > handle(const seastar::sstring& path,
            std::unique_ptr<request> req, std::unique_ptr<reply> rep) override {
        rep->set_mime_type("text/plain; charset=utf-8");
        seastar::sstring key_id;
        try {
            key_id = req->param["key_id"];
            if (key_id.find('%'))
                key_id = url_decode(key_id);
            if (key_id.length() > 255) {
                rep->_status = reply::status_type::bad_request;
                co_return std::move(rep);
            }
        }
        catch (const std::exception& /*e*/) {
            rep->_status = reply::status_type::bad_request;
            co_return std::move(rep);
        }
        switch (seastar::httpd::str2type(req->_method)) {
        case seastar::httpd::operation_type::GET: {
            seastar::sstring content;
            bool res = co_await _storage.get(key_id, content);
            if (res) {
                rep->_content = content;
                rep->_status = reply::status_type::ok;
            } else {
                rep->_status = reply::status_type::not_found;
            }
            break;
        }
        case seastar::httpd::operation_type::PUT: {
            bool res = co_await _storage.add(key_id, req->content);
            if (res) {
                rep->_status = reply::status_type::ok;
            } else {
                rep->_status = reply::status_type::not_found;
            }
            break;
        }
        case seastar::httpd::operation_type::DELETE: {
            bool res = co_await _storage.remove(std::move(key_id));
            if (res) {
                rep->_status = reply::status_type::ok;
            } else {
                rep->_status = reply::status_type::not_found;
            }
            break;
        }
        default: {
            rep->_status = reply::status_type::method_not_allowed;
            break;
        }
        }
        co_return std::move(rep);
    }
};

class keys_handler : public seastar::httpd::handler_base {
    sharded_storage& _storage;
public:
    keys_handler(sharded_storage& storage) : _storage(storage) {}

    seastar::future<std::unique_ptr<reply> > handle(const seastar::sstring& path,
            std::unique_ptr<request> req, std::unique_ptr<reply> rep) override {
        rep->set_mime_type("text/plain; charset=utf-8");
        switch (seastar::httpd::str2type(req->_method)) {
        case seastar::httpd::operation_type::GET: {
            seastar::sstring content;
            seastar::sstring list = co_await _storage.list();
            rep->_content = std::move(list);
            rep->_status = reply::status_type::ok;
            break;
        }
        case seastar::httpd::operation_type::DELETE: {
            rep->_status = reply::status_type::ok;
            break;
        }
        default: {
            rep->_status = reply::status_type::method_not_allowed;
            break;
        }
        }
        co_return std::move(rep);
    }
};

class stop_signal {
    bool _caught = false;
    seastar::condition_variable _cond;
private:
    void signaled() { if (_caught) { return; } _caught = true; _cond.broadcast(); }
public:
    stop_signal() { seastar::engine().handle_signal(SIGINT, [this] { signaled(); }); seastar::engine().handle_signal(SIGTERM, [this] { signaled(); }); }
    ~stop_signal() { seastar::engine().handle_signal(SIGINT, [] {}); seastar::engine().handle_signal(SIGTERM, [] {}); }
    seastar::future<> wait() { return _cond.wait([this] { return _caught; }); }
    bool stopping() const { return _caught; }
};

int main(int argc, char** argv) {
    constexpr size_t cache_limit = 4000000;
    seastar::distributed<storage> storage_peers;
    sharded_storage storage(storage_peers, cache_limit);
    namespace bpo = boost::program_options;
    seastar::httpd::http_server_control server_control;
    seastar::app_template app;
    app.add_options()("port", bpo::value<uint16_t>()->default_value(10000), "HTTP Server port");
    try {
        app.run(argc, argv, [&] {
            return seastar::async([&] () {
                stop_signal stopsignal;
                auto&& config = app.configuration();
                uint16_t port = config["port"].as<uint16_t>();
                storage_peers.start(std::move(cache_limit)).get();
                storage_peers.invoke_on_all(&storage::init).get();
                // storage data(cache_limit);
                // test_storage(data).get();
                server_control.start().get();
                server_control.set_routes([&](seastar::httpd::routes& r) {
                    r.add(seastar::httpd::operation_type::GET, seastar::httpd::url("/keys"), new keys_handler(storage));
                    r.add(seastar::httpd::operation_type::GET, seastar::httpd::url("/key").remainder("key_id"), new key_handler(storage));
                    r.add(seastar::httpd::operation_type::PUT, seastar::httpd::url("/key").remainder("key_id"), new key_handler(storage));
                    r.add(seastar::httpd::operation_type::DELETE, seastar::httpd::url("/key").remainder("key_id"), new key_handler(storage));
                }).get();
                auto stop_server = seastar::defer([&] () noexcept {
                    std::cout << "Stoppping cache app" << std::endl;
                    server_control.stop().get();
                    storage_peers.stop().get();
                });
                server_control.listen(port).get();
                std::cout << "Seastar HTTP server listening on port " << port << " ..." << std::endl;
                stopsignal.wait().get();
                return 0;
            });
        });
    } catch(...) {
        std::cerr << "Couldn't start application: " << std::current_exception() << std::endl;
        return 1;
    }
    return 0;
}

